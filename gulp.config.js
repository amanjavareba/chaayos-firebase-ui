module.exports = function() {
    var src = 'src',
        clientApp = 'src/app',
        clientMock = 'src/app/mockbackend',
        dist = 'dist',
        tmp = '.tmp',
        prodUrl='http://localhost:8080'
        docs = 'documentation',
        landing = 'landing',
        url = '';
    var config = {
        src: src,
        clientApp:clientApp,
        proxy:{
            production:{
                url:prodUrl,
                route:"/api",
                secure:false,
                host:"proxy",
                  pathRewrite: {
                    '^/api/password' : 'password'
                }
            },
            dev:{
                url:"http://localhost:3000",
                route:"/api",
                secure:false,
                host:"proxy",
                  pathRewrite: {
                    '^/api/password' : 'password'
                    
                }
            }
        },
        dist: dist,
        tmp: tmp,
        index: src + "/index.html",
        alljs: [
            src + "/app/**/*.js",
            './*.js'
        ],
        assets: [
            src + "/app/**/*.html",
            src + "/bower_components/font-awesome/css/*", 
            src + "/bower_components/font-awesome/fonts/*", 
            src + "/bower_components/weather-icons/css/*", 
            src + "/bower_components/weather-icons/font/*", 
            src + "/bower_components/weather-icons/fonts/*", 
            src + "/bower_components/ionicons/css/*",
            src + "/bower_components/ionicons/fonts/*",
            src + "/bower_components/font-awesome-animation/dist/*",
            src + "/bower_components/angular-animate/*",
            src + "/bower_components/jasny-bootstrap/dist/css/*",
            src + "/bower_components/jasny-bootstrap/dist/js/*",
            src + "/bower_components/material-design-iconic-font/dist/**/*",
	    src + "/bower_components/country-select-js/build/css/countrySelect.min.css",
	    src + "/bower_components/simple-line-icons/css/simple-line-icons.css",
            src + "/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css",
            src + "/bower_components/select2/dist/css/select2.min.css",
            src + "/bower_components/select2-bootstrap/select2-bootstrap.css",
	    src + "/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.css",
	    src + "/bower_components/intl-tel-input/build/css/intlTelInput.css",
	    src + "/bower_components/toastr/toastr.min.css",
	    src + "/bower_components/angular-bootstrap-grid-tree/src/treeGrid.css",
	    src + "/bower_components/angular-ivh-treeview/dist/ivh-treeview.css",
	    src + "/bower_components/angular-dialog-service/dist/dialogs.css",
	    src + "/bower_components/simple-line-icons/fonts/Simple-Line-Icons.woff2",

	    src + "/styles/bootstrap.css",
            src + "/assets/**/*",
            src + "/styles/loader.css", 
            src + "/styles/ui/images/*", 
            src + "/favicon.ico"
        ],
        less: [],
        sass: [
            src + "/styles/**/*.scss"
        ],
	compiledsass: [
	    tmp + "/styles/**/*.css"	
	],
        js: [
            clientApp + "/**/*.module.js",
            clientApp + "/**/*.js",
            '!' + clientApp + "/**/*.spec.js",
            '!' + clientMock + "/**/*.js"
        ],
	copyjs:[
	    src + "/bower_components/angular-bootstrap-grid-tree/src/tree-grid-directive.js",	
	    src + "/bower_components/angular-dialog-service/dist/dialogs-default-translations.js",
	    src + "/bower_components/angular-dialog-service/dist/dialogs.js"
	],
	fonts: [
            src + "/fonts/**/*"		
	],
        mockjs: [
            clientApp + "/**/*.module.js",
            clientApp + "/**/*.js",
            src + "/bower_components/angular-mocks/angular-mocks.js",
            '!' + clientApp + "/**/*.spec.js"
        ],
        
        docs: docs, 
        docsJade: [
            docs + "/jade/index.jade",
            docs + "/jade/faqs.jade",
            docs + "/jade/layout.jade"
        ],
        allToClean: [
            tmp, 
            ".DS_Store",
            ".sass-cache",
            "node_modules",
            ".git",
            src + "/bower_components",
            docs + "/jade",
            docs + "/layout.html",
            landing + "/jade",
            landing + "/bower_components",
            "readme.md"
        ]
    };

    return config;
};
