angular.module('app').config(['$routeProvider','$httpProvider','$locationProvider',function($routeProvider,$httpProvider,$locationProvider) {
    $routeProvider
    .when("/", {redirectTo : "/transaction"})
    .when("/transaction",{templateUrl: "app/demo/transaction.html"})
    

    $locationProvider.html5Mode({
                  enabled: true,
                  requireBase: false
                });
}]);