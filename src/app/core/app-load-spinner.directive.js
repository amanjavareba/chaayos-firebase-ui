// Route State Load Spinner(used on page or content load)
angular.module('app').directive('ngAppLoadSpinner', ['$rootScope', '$document', '$location',
    function($rootScope, $document, location) {
        return {
            restrict: 'EA',
            link: function(scope, element, attrs , $location) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$routeChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                    // Layout.closeMainMenu();
                });

                $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
                    element.addClass('hide'); // hide spinner bar
                    angular.element('body').removeClass('page-on-load'); // remove page loading indicator
                    // Layout.setAngularJsMainMenuActiveLink('match', null, event.currentScope.$state); // activate selected link in the sidebar menu
                    
                    // auto scorll to page top
                    setTimeout(function () {
                        $document.scrollTop(); // scroll to the top on content load
                    });    

                  
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$routeChangeError', function() {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
]);
