(function () {
    'use strict';

    angular.module('app', [
        // Angular modules
        'ngRoute',
        'ngAnimate',
        'ngAria',
        'ngResource',
        'ngCookies',
        
        // 3rd Party Modules
        'firebase',
        'toaster',
        
        // Custom modules
        'app.demo'
        
        ])
})();