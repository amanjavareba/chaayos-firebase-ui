angular.module('app.demo').controller('transactionCtrl',["$scope","TransectionService","toaster",
	function($scope,TransectionService,toaster){
		var self=this;
		self.transections=[]
		self.initial=true;
		self.nextId=0
		self.db = firebase.firestore()
        self.collection=self.db.collection("chaayosAssignment")
    	self.transection={
    		"id":"",
    		"msg":"",
    		"amount":""
    	};

    	self.collection.orderBy("id").onSnapshot(function(querySnapshot) {
	        self.transections=[];
	        self.nextId=querySnapshot.docs.length+1;
	        querySnapshot.forEach(function(doc) {
	           if(doc.data().id!=undefined){
		            self.transections.push({
		            	"id":doc.data().id,
		            	"msg":doc.data().msg,
		            	"amount":doc.data().amount
		            })
	        	}
	        });
	        $scope.$apply(function() {
				    self.transections;
				    if((!self.initial&&self.transections!=0))
				    {
				    	toaster.pop('info', "new transection");
				    }else 
				    {
				    	self.initial=false		
				    }
				    
			});
			
	        console.log(self.transections,self.nextId)
    	})
 

    	self.generateTransection=function(trans){
    		trans.id=self.nextId;
    		console.log(trans)
    		TransectionService.insert(trans).then(function(resp){
    			//console.log(resp)

    		})
    	}
	}
]);



// angular.module('app.demo').controller('transactionCtrl',[
// 	function(){
// 		var self=this;
// 		self.transections=[]
// 		self.db = firebase.firestore()
//         self.collection=self.db.collection("chaayosAssignment")
    
//     	self.collection.onSnapshot(function(querySnapshot) {
//         querySnapshot.forEach(function(doc) {
//             self.transections.push({
//             	"name":doc.data().name,
//             	"value":doc.data().field
//             })
//             // cities.push({"name":doc.data().name,"value":doc.data().field});
//         });
//         console.log(self.transections)
//         //console.log(cities);
//     });
// 	}
// ]);